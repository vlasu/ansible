listen { 
    port = 4040 
    address = "127.0.0.1" 
}

namespace "nginx" {
    source = { 
        files = [
            "/var/log/nginx/access.log" 
        ]
    }

    format = "$remote_addr $realip_remote_addr $remote_user [$time_local] $host \"$request\" 
$status $bytes_sent $request_length $http_referer \"$http_user_agent\" {$upstream_addr}
$request_time $upstream_response_time $scheme $upstream_http_x_request_id"

    labels { 
        app = ”prometheus"
    }

    histogram_buckets = [.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10, 20, 30]
}